package com.kerong.chedeli.data.modal;

import java.util.List;

/**
 * Created by lyh on 15/10/16.
 */
public class CarSimpleListResult  {
    private int status;
    private String message;
    private int total;
    private List<CarSimpleModal> results;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CarSimpleModal> getResults() {
        return results;
    }

    public void setResults(List<CarSimpleModal> results) {
        this.results = results;
    }
}
