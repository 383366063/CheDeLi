package com.kerong.chedeli.view.launch;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.kerong.chedeli.CheDeLiApplication;
import com.kerong.chedeli.R;
import com.kerong.chedeli.data.modal.UserModal;
import com.kerong.chedeli.event.LoginEvent;
import com.kerong.chedeli.utils.PreferencesUtils;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;
import com.orhanobut.logger.Logger;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by LiYaoHua on 2015/9/21.
 */
public class LaunchActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFullScreen();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        initData();
    }

    private void initData(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //根据用户是不是第一次打开APP来进入不同的界面
                if(PreferencesUtils.getBoolean(LaunchActivity.this, CheDeLiApplication.PRE_FIRST_OPEN,true)){
                    initGuide();
                }else{
                    //执行登录

                    UIHelper.showHome(LaunchActivity.this, true);
                }

            }
        }, 2000);

    }

    /**
     * 到引导界面
     */
    private void initGuide(){
        UIHelper.showGuide(this,true);
        PreferencesUtils.putBoolean(this, CheDeLiApplication.PRE_FIRST_OPEN, false);
    }



    /**
     * 事件消费
     * @param event
     */
    public void onEvent(LoginEvent event){
        Logger.d("登录成功状态%s,失败消息%s",new Object[]{event.isStatus(),event.getMsg()});
        UIHelper.showHome(this, true);
        /*if(event.isStatus()){
            UIHelper.showHome(this, true);
        }else{
            if(null!=event.getMsg()){
                UIHelper.showMessage(event.getMsg());
            }
            UIHelper.showLogin(this,true);
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
