package com.kerong.chedeli.view.driver;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.kerong.chedeli.R;
import com.kerong.chedeli.view.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by LiYaoHua on 2015/10/7.
 */
public class EditCarActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcar);
        ButterKnife.bind(this);
        initToolBar(toolbar);
        init();
    }

    private void init(){

    }

    @OnClick(R.id.bt_send_car)
    public void onSendClick(){
        finish();
    }
}
