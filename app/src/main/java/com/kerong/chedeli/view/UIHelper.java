package com.kerong.chedeli.view;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.kerong.chedeli.CheDeLiApplication;
import com.kerong.chedeli.view.home.MainActivity;
import com.kerong.chedeli.view.home.MainNewActivity;
import com.kerong.chedeli.view.home.SearchActivity;
import com.kerong.chedeli.view.launch.GuideActivity;
import com.kerong.chedeli.view.user.LoginActivity;

/**
 * Created by LiYaoHua on 2015/9/21.
 */
public class UIHelper  {

    /**
     * 到主界面
     * @param context
     */
    public static void showHome(Activity context,boolean isfinish){
        Intent intent = new Intent(context, MainNewActivity.class);
        context.startActivity(intent);
        if(isfinish){
            context.finish();
        }
    }

    /**
     * 到引导界面
     * @param context
     */
    public static void showGuide(Activity context,boolean isfinish){
        Intent intent = new Intent(context, GuideActivity.class);
        context.startActivity(intent);
        if(isfinish){
            context.finish();
        }
    }

    /**
     * 到登录界面
     * @param context
     * @param isfinish
     */
    public static void showLogin(Activity context,boolean isfinish){
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        if(isfinish){
            context.finish();
        }
    }

    /**
     * 到登录界面
     * @param context
     * @param isfinish
     */
    public static void showSearch(Activity context,boolean isfinish){
        Intent intent = new Intent(context, SearchActivity.class);
        context.startActivity(intent);
        if(isfinish){
            context.finish();
        }
    }

    /**
     * 消息提示封装
     * @param msg
     */
    public static void showMessage(String msg){
        Toast.makeText(CheDeLiApplication.getInstance(), msg, Toast.LENGTH_SHORT).show();
    }
}
