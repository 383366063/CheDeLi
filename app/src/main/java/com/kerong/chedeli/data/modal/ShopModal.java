package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 网点对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class ShopModal implements Parcelable {
    private String shopid;
    private String shop_name;
    private String carcount;
    private String shop_phone;
    private String address;
    private LocationModal location;
    private String distance;

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getCarcount() {
        return carcount;
    }

    public void setCarcount(String carcount) {
        this.carcount = carcount;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocationModal getLocation() {
        return location;
    }

    public void setLocation(LocationModal location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.shopid);
        dest.writeString(this.shop_name);
        dest.writeString(this.carcount);
        dest.writeString(this.shop_phone);
        dest.writeString(this.address);
        dest.writeParcelable(this.location, 0);
        dest.writeString(this.distance);
    }

    public ShopModal() {
    }

    protected ShopModal(Parcel in) {
        this.shopid = in.readString();
        this.shop_name = in.readString();
        this.carcount = in.readString();
        this.shop_phone = in.readString();
        this.address = in.readString();
        this.location = in.readParcelable(LocationModal.class.getClassLoader());
        this.distance = in.readString();
    }

    public static final Parcelable.Creator<ShopModal> CREATOR = new Parcelable.Creator<ShopModal>() {
        public ShopModal createFromParcel(Parcel source) {
            return new ShopModal(source);
        }

        public ShopModal[] newArray(int size) {
            return new ShopModal[size];
        }
    };
}
