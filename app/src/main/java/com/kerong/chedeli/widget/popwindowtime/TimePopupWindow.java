package com.kerong.chedeli.widget.popwindowtime;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import com.kerong.chedeli.R;
import com.kerong.chedeli.widget.popwindowtime.lib.ScreenInfo;
import com.kerong.chedeli.widget.popwindowtime.lib.WheelTime;
import com.orhanobut.logger.Logger;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * 时间选择器
 * 
 * @author Sai
 * 
 */
@SuppressLint("InflateParams")
public class TimePopupWindow extends PopupWindow implements OnClickListener {
	public enum Type {
		ALL, YEAR_MONTH_DAY, HOURS_MINS, MONTH_DAY_HOUR_MIN, CUSTOM_DATE_TIME
	}// 四种选择模式，年月日时分，年月日，时分，月日时分 ,自定义时间

	private View rootView; // 总的布局
	WheelTime wheelTime;
	private Button bt_loan_time, bt_back_time;
	private ImageButton bt_delete;
	private TextView next_step;
	private static final String TAG_SUBMIT = "submit";
	private static final String TAG_CANCEL = "cancel";
	private OnTimeSelectListener timeSelectListener;
	private Date loanTime;//借车时间
	private Date backTime;//还车时间
	private Context context;
	@SuppressWarnings("deprecation")
	public TimePopupWindow(Context context, Type type) {
		super(context);
		this.context = context;
		this.setWidth(LayoutParams.MATCH_PARENT);
		this.setHeight(LayoutParams.WRAP_CONTENT);
		this.setBackgroundDrawable(new BitmapDrawable());// 这样设置才能点击屏幕外dismiss窗口
		this.setOutsideTouchable(true);
		//this.setAnimationStyle(R.style.timepopwindow_anim_style);
		loanTime = new Date();
		LayoutInflater mLayoutInflater = LayoutInflater.from(context);
		rootView = mLayoutInflater.inflate(R.layout.popwindow_use_time, null);
		// -----借车时间和还车时间按钮
		bt_loan_time = (Button) rootView.findViewById(R.id.bt_loan_time);
		bt_back_time = (Button) rootView.findViewById(R.id.bt_back_time);
		bt_loan_time.setOnClickListener(this);
		bt_back_time.setOnClickListener(this);
		// ----时间转轮
		final View timepickerview = rootView.findViewById(R.id.timepicker);
		ScreenInfo screenInfo = new ScreenInfo((Activity) context);
		wheelTime = new WheelTime(timepickerview, type);
		//取消和下一步按钮
		bt_delete = (ImageButton) rootView.findViewById(R.id.bt_delete);
		bt_delete.setTag(TAG_CANCEL);
		bt_delete.setOnClickListener(this);
		next_step = (TextView) rootView.findViewById(R.id.next_step);
		next_step.setTag(TAG_SUBMIT);
		next_step.setOnClickListener(this);
		wheelTime.screenheight = screenInfo.getHeight();

		//默认选中当前时间
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		if(!type.toString().equals(Type.CUSTOM_DATE_TIME.toString())){
			System.out.println("type.toString()="+type.toString());
			wheelTime.setPicker(year, month, day, hours, minute);
		}
		
		setContentView(rootView);
	}

	/**
	 * 设置可以选择的时间范围
	 * 
	 * @param START_YEAR
	 * @param END_YEAR
	 */
	public void setRange(int START_YEAR, int END_YEAR) {
		WheelTime.setSTART_YEAR(START_YEAR);
		WheelTime.setEND_YEAR(END_YEAR);
	}
	
	/**
	* @Title: setCustomDateTime 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* minMon
	* maxMon
	* minDay
	* maxDay
	* minHour
	* maxHour
	* halfTime    
	* void    返回类型 
	 */
	public void setCustomDateTime(int minMon,int maxMon,int minDay,int maxDay
			,int minHour, int maxHour,Integer[] halfTime){
		wheelTime.setCustomPicker(minMon, maxMon, minDay, maxDay, minHour, maxHour, halfTime);
	}
	
	/**
	 * 设置选中时间
	 * @param date
	 */
	public void setTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date == null)
			calendar.setTimeInMillis(System.currentTimeMillis());
		else
			calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		wheelTime.setPicker(year, month, day, hours, minute);
	}

	/**
	 * 指定选中的时间，显示选择器
	 * 
	 * @param parent
	 * @param gravity
	 * @param x
	 * @param y
	 * @param date
	 */
	public void showAtLocation(View parent, int gravity, int x, int y, Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date == null)
			calendar.setTimeInMillis(System.currentTimeMillis());
		else
			calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		wheelTime.setPicker(year, month, day, hours, minute);
		update();
		super.showAtLocation(parent, gravity, x, y);
	}

	/**
	 * 设置是否循环滚动
	 * 
	 * @param cyclic
	 */
	public void setCyclic(boolean cyclic) {
		wheelTime.setCyclic(cyclic);
	}

	@Override
	public void onClick(View v) {
		String tag = (String) v.getTag();
		if (null!=tag && tag.equals(TAG_CANCEL)) {
			dismiss();
			return;
		}
		Date date = null;
		try {
			date = WheelTime.dateFormat.parse(wheelTime.getTime());
		} catch (ParseException e) {
			Logger.i("时间格式转化错误:%s",e.getMessage());
		}

		//判断是哪个按钮
		if(v.getId() == R.id.bt_loan_time){//借车时间
			bt_loan_time.setBackgroundResource(R.drawable.arrow_1);
			bt_back_time.setBackgroundResource(R.drawable.arrow_3);
			bt_loan_time.setTextColor(context.getResources().getColor(android.R.color.white));
			bt_back_time.setTextColor(context.getResources().getColor(R.color.txt_color_title_green));
			if(loanTime!=null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(loanTime);
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH);
				int day = calendar.get(Calendar.DAY_OF_MONTH);
				int hours = calendar.get(Calendar.HOUR_OF_DAY);
				int minute = calendar.get(Calendar.MINUTE);
				wheelTime.setPicker(year, month, day, hours, minute);
				update();
			}
			next_step.setText("下一步");
			return;
		}else if(v.getId() == R.id.bt_back_time){//还车时间
			bt_back_time.setBackgroundResource(R.drawable.arrow_2);
			bt_loan_time.setBackgroundResource(R.drawable.arrow_3);
			bt_back_time.setTextColor(context.getResources().getColor(android.R.color.white));
			bt_loan_time.setTextColor(context.getResources().getColor(R.color.txt_color_title_green));
			if(backTime!=null){
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(backTime);
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH);
				int day = calendar.get(Calendar.DAY_OF_MONTH);
				int hours = calendar.get(Calendar.HOUR_OF_DAY);
				int minute = calendar.get(Calendar.MINUTE);
				wheelTime.setPicker(year, month, day, hours, minute);
				update();
			}
			next_step.setText("完成");
			return;
		}else if(v.getId() == R.id.next_step){
			if(next_step.getText().equals("下一步")){
				bt_back_time.setBackgroundResource(R.drawable.arrow_2);
				bt_loan_time.setBackgroundResource(R.drawable.arrow_3);
				bt_back_time.setTextColor(context.getResources().getColor(android.R.color.white));
				bt_loan_time.setTextColor(context.getResources().getColor(R.color.txt_color_title_green));
				loanTime = date;
				next_step.setText("完成");
				return;
			}else{
				backTime = date;
				if (timeSelectListener != null) {
					try {
						//Date date = WheelTime.dateFormat.parse(wheelTime.getTime());
						timeSelectListener.onTimeSelect(loanTime,backTime);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(null!=onComfirmListener){
					onComfirmListener.onTimeConfirm(wheelTime.getTimeArray());
				}

				dismiss();
				return;
			}
		}
	}

	public interface OnTimeSelectListener {
		public void onTimeSelect(Date loanTime,Date backTime);
	}

	public void setOnTimeSelectListener(OnTimeSelectListener timeSelectListener) {
		this.timeSelectListener = timeSelectListener;
	}
	
	private OnTimeComfirmListener onComfirmListener;
	public interface OnTimeComfirmListener{
		public void onTimeConfirm(String[] time);
	}
	
	public void setOnTimeComfirmListener(OnTimeComfirmListener onComfirmListener) {
		this.onComfirmListener = onComfirmListener;
	}

}
