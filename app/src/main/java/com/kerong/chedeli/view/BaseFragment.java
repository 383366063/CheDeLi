package com.kerong.chedeli.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Fragment的基类
 * Created by LiYaoHua on 2015/9/21.
 */
public class BaseFragment extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    protected void init(){}
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    protected void turnToActivity(Class<? extends Activity> activity){
        Intent intent = new Intent(getActivity(),activity);
        startActivity(intent);
    }

    protected void turnToActivityByFinish(Class<? extends Activity> activity){
        Intent intent = new Intent();
        intent.setClass(getActivity(),activity);
        startActivity(intent);
        getActivity().finish();
    }

    protected  void turnToActivity(Class<? extends Activity> activity,Intent intent){
        intent.setClass(getActivity(), activity);
        startActivity(intent);
    }
    protected void turnToActivityByFinish(Class<? extends Activity> activity,Intent intent){
        intent.setClass(getActivity(),activity);
        startActivity(intent);
        getActivity().finish();
    }
}
