package com.kerong.chedeli.view.user;

import com.google.gson.Gson;
import com.kerong.chedeli.CheDeLiApplication;

import com.kerong.chedeli.R;

import com.kerong.chedeli.data.modal.UserModal;
import com.kerong.chedeli.data.modal.UserResult;
import com.kerong.chedeli.utils.PreferencesUtils;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;
import com.kerong.chedeli.view.home.MainActivity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.apache.http.impl.client.DefaultHttpClient;

/**
 * 登录界面 Created by LiYaoHua on 2015/9/21.
 */
public class LoginActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.et_phone)
    EditText et_phone;
    @Bind(R.id.et_password)
    EditText et_password;
    @Bind(R.id.tv_forget_password)
    TextView tv_forget_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initToolBar(toolbar);

        UserModal userModal = CheDeLiApplication.getUserMoal();
        if(userModal != null){
            if (!TextUtils.isEmpty(userModal.getUsrmobile())) {
                et_phone.setText(userModal.getUsrmobile());
                et_password.setText(userModal.getUsrpass());
            }
        }
    }

    @OnClick(R.id.bt_login)
    public void onLoginClick(View login) {
        final String usrmobile = et_phone.getText().toString().trim();
        final String usrpass = et_password.getText().toString().trim();
        if (TextUtils.isEmpty(usrmobile)) {
            et_phone.setError("手机号不能为空");
            return;
        }
        if (TextUtils.isEmpty(usrpass)) {
            et_password.setError("密码不能为空");
            return;
        }
        AjaxParams params = new AjaxParams();
        params.put("usrmobile", usrmobile);
        params.put("usrpass", usrpass);
        params.put("phoneos", "2");
        String url = "http://cdlmapi.0371e.cn/v1/login";
        final FinalHttp finalHttp = new FinalHttp();
        finalHttp.get(url, params, new AjaxCallBack<String>() {
            @Override
            public void onSuccess(String s) {
                try {
                    UserResult userResult = new Gson().fromJson(s,UserResult.class);
                    if(userResult.getStatus() == 0){
                        DefaultHttpClient httpClient = (DefaultHttpClient) finalHttp.getHttpClient();
                        CheDeLiApplication.cookieStore = httpClient.getCookieStore();
                        UserModal userModal = userResult.getResults();
                        userModal.setUsrmobile(usrmobile);
                        userModal.setUsrpass(usrpass);
                        CheDeLiApplication.saveUserModal(userModal);
                        UIHelper.showMessage("登陆成功");
                        PreferencesUtils.putBoolean(LoginActivity.this,
                                CheDeLiApplication.PRE_LOGIN_STATUS, true);
                        EventBus.getDefault().post("loginsuccess");
                        finish();
                    }else{
                        UIHelper.showMessage(userResult.getMessage());
                    }
                }catch (Exception e){
                    UIHelper.showMessage("登陆失败");
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                UIHelper.showMessage(strMsg);
            }
        });
    }
    @OnClick(R.id.tv_forget_password)
    public void onFindPWDClick(View register) {
        turnToActivityByFinish(FindPWDActivity.class);
    }

    @OnClick(R.id.tv_register)
    public void onRegisterClick(View register) {
        turnToActivityByFinish(RegisterActivity.class);
    }


}
