package com.kerong.chedeli.widget.priceseekbar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;

import com.kerong.chedeli.R;

import java.math.BigDecimal;

/**
 * 横向价格选择器
 * Created by LiYaoHua on 2015/9/23.
 */
public class HScrollPriceSeekBar extends View{
    private static final String TAG = "HScrollPriceSeekBar";
    private static final int CLICK_ON_LOW = 1;//点击前面的滑块
    private static final int CLICK_ON_HIGH = 2;//点击后面的滑块
    private static final int CLICK_IN_LOW_AREA = 3;//
    private static final int CLICK_IN_HIGH_AREA = 4;
    private static final int CLICK_OUT_AREA = 5;
    private static final int CLICK_INVAILD = 0;
    private static final int[] STATE_NORMAL = {};
    private static final int[] STATE_PRESSED = {
            android.R.attr.state_pressed, android.R.attr.state_window_focused
    };
    private Drawable hasScrollBarBg;    //滑动条滑动后的背景
    private Drawable notScrollBarBg;    //滑动条滑动前的背景
    private Drawable lowScrollTextBg;   // 前面一个滑块的文字的背景图
    private Drawable highScrollTextBg;  //后一个滑块的文字的背景图
    private Drawable mThumbLow;         //前滑块
    private Drawable mThumbHigh;        //后滑块

    private int mScrollBarWidth;    //控件宽度=滑动条宽度+滑块宽度
    private int mScrollBarHeight;   //滑动条高
    private int mThumbWidth;        //滑动块宽度
    private int mThumbHeight;       //滑动块高度
    private double mOffSetLow = 0;  //前滑块中心坐标
    private double mOffSetHigh = 0; //后滑块中心坐标
    private int mDistance = 0;     //总刻度是固定距离 两边个去掉滑块距离

    private int mThumbMarginTop = 100;//滑块顶部距离上边距离 距离字体顶部的距离

    private int mFlag = CLICK_INVAILD;
    private OnSeekBarChangeListener mBarChangeListener;

    private double defaultScreenLow = 0;//默认前滑块位置百分比
    private double defaultScreenHigh = 100; //默认后滑块位置百分比
    private Paint paint = null;
    public HScrollPriceSeekBar(Context context) {
        this(context,null);
    }

    public HScrollPriceSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HScrollPriceSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Resources resources = getResources();
        paint = new Paint();
        paint.setAntiAlias(true);
        hasScrollBarBg = resources.getDrawable(R.mipmap.ic_scrollbar_bg);
        notScrollBarBg = resources.getDrawable(R.mipmap.ic_scrollbar_bg_normal);
        lowScrollTextBg = resources.getDrawable(R.mipmap.chat_user);
        highScrollTextBg = resources.getDrawable(R.mipmap.chat_user);
        mThumbLow = resources.getDrawable(R.mipmap.car_level_limit);
        mThumbHigh = resources.getDrawable(R.mipmap.car_level_limit);
        mThumbLow.setState(STATE_NORMAL);
        mThumbHigh.setState(STATE_NORMAL);
        mScrollBarWidth = notScrollBarBg.getIntrinsicWidth();
        mScrollBarHeight = notScrollBarBg.getIntrinsicHeight();
        mThumbWidth = mThumbLow.getIntrinsicWidth();
        mThumbHeight = mThumbLow.getIntrinsicHeight();
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //获得宽度
        int width = measureWidth(widthMeasureSpec);
        mScrollBarWidth = width;
        mOffSetHigh = width - mThumbWidth/2;
        mOffSetLow = mThumbWidth/2;
        mDistance = width - mThumbWidth;
        mOffSetLow = formatDouble(defaultScreenLow/100*(mDistance))+mThumbWidth/2;
        mOffSetHigh = formatDouble(defaultScreenHigh/100*(mDistance))+mThumbWidth/2;
        setMeasuredDimension(width, mThumbHeight + mThumbMarginTop);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
    }

    private int measureWidth(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        //wrap_content
        if (specMode == MeasureSpec.AT_MOST) {
        }
        //fill_parent或者精确值
        else if (specMode == MeasureSpec.EXACTLY) {
        }

        return specSize;
    }

    private int measureHeight(int measureSpec) {
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        int defaultHeight = 100;
        //wrap_content
        if (specMode == MeasureSpec.AT_MOST) {
        }
        //fill_parent或者精确值
        else if (specMode == MeasureSpec.EXACTLY) {
            defaultHeight = specSize;
        }

        return defaultHeight;
    }

    /**
     * double保留2为小数
     * @param pDouble
     * @return
     */
    public static double formatDouble(double pDouble) {
        BigDecimal bd = new BigDecimal(pDouble);
        BigDecimal bd1 = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        pDouble = bd1.doubleValue();
        return pDouble;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //获得前后滑块的距离
        double progressLow = formatDouble((mOffSetLow - mThumbWidth / 2) * 100 / mDistance);
        double progressHigh = formatDouble((mOffSetHigh - mThumbWidth / 2) * 100 / mDistance);
        if(progressLow==0 || progressLow == 100){
            mThumbLow = getResources().getDrawable(R.mipmap.car_level_limit);
        }else if(progressLow<=10 && progressLow>0){
            mThumbLow = getResources().getDrawable(R.mipmap.car_level_0);
        }else if(progressLow>25 && progressLow<=50){
            mThumbLow = getResources().getDrawable(R.mipmap.car_level_1);
        }else if(progressLow > 50 && progressLow <100){
            mThumbLow = getResources().getDrawable(R.mipmap.car_level_2);
        }

        if(progressHigh==0 || progressHigh == 100){
            mThumbHigh = getResources().getDrawable(R.mipmap.car_level_limit);
        }else if(progressHigh<=10 && progressHigh>0){
            mThumbHigh = getResources().getDrawable(R.mipmap.car_level_0);
        }else if(progressHigh>25 && progressHigh<=50){
            mThumbHigh = getResources().getDrawable(R.mipmap.car_level_1);
        }else if(progressHigh > 50 && progressHigh <100){
            mThumbHigh = getResources().getDrawable(R.mipmap.car_level_2);
        }
        //字体样式
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(10 * getResources().getDisplayMetrics().density);
        //获得滑动条背景图的顶部边界和底部边界 用户定位
        int scroll_top = mThumbMarginTop + mThumbHeight/2 - mScrollBarHeight/2;
        int scroll_bottom = scroll_top + mScrollBarHeight;
        //绘制滑动前的背景
        notScrollBarBg.setBounds(mThumbWidth/2,scroll_top,mScrollBarWidth - mThumbWidth/2,scroll_bottom);
        notScrollBarBg.draw(canvas);
        //绘制滑动后的背景
        hasScrollBarBg.setBounds((int) mOffSetLow, scroll_top, (int) mOffSetHigh, scroll_bottom);
        hasScrollBarBg.draw(canvas);
        //绘制前面的滑动块
        mThumbLow.setBounds((int) (mOffSetLow - mThumbWidth / 2),
                mThumbMarginTop, (int) (mOffSetLow + mThumbWidth / 2), mThumbHeight + mThumbMarginTop);
        mThumbLow.draw(canvas);
        //绘制后面的滑动块
        mThumbHigh.setBounds((int) (mOffSetHigh - mThumbWidth / 2), mThumbMarginTop,
                (int) (mOffSetHigh + mThumbWidth / 2), mThumbHeight + mThumbMarginTop);
        mThumbHigh.draw(canvas);

        //设置前滑块的距离
        lowScrollTextBg.setBounds((int) mOffSetLow-lowScrollTextBg.getIntrinsicWidth()/2,
                mThumbMarginTop-lowScrollTextBg.getIntrinsicHeight(),
                (int)(mOffSetLow +lowScrollTextBg.getIntrinsicWidth()/2), mThumbMarginTop);
        lowScrollTextBg.draw(canvas);
        //绘制滑块头上的文本
        paint.setColor(Color.WHITE);
        int jiage = 0;
        String price_low = "";
        if(progressLow<50){
            jiage = (int)progressLow*20;
            price_low = "￥ "+jiage+"元";
        }else if(progressLow>50 && progressLow <60){
            jiage = 1000;
            price_low = "￥ "+jiage+"元";
        }else if(progressLow>60 && progressLow <100){
            jiage = ((int)(progressLow*2-100)/10)*1000;
            price_low = "￥ "+jiage+"元";
        }else{
            price_low = "不限";
        }
        canvas.drawText(price_low + "", (int) mOffSetLow,mThumbMarginTop-lowScrollTextBg.getIntrinsicHeight()/2+5, paint);
        //绘制后滑块
        highScrollTextBg.setBounds((int) mOffSetHigh-lowScrollTextBg.getIntrinsicWidth()/2,
                mThumbMarginTop-lowScrollTextBg.getIntrinsicHeight(),
                (int)(mOffSetHigh + lowScrollTextBg.getIntrinsicWidth()/2), mThumbMarginTop);
        highScrollTextBg.draw(canvas);
        //绘制后滑块的文本
        int high_jiage = 0;
        String price_high = "";
        if(progressHigh<50){
            high_jiage = (int)progressHigh*20;
            price_high = "￥ "+high_jiage+"元";
        }else if(progressHigh>50 && progressHigh <60){
            high_jiage = 1000;
            price_high = "￥ "+high_jiage+"元";
        }else if(progressHigh>60 && progressHigh <100){
            high_jiage = ((int)(progressHigh*2-100)/10)*1000;
            price_high = "￥ "+high_jiage+"元";
        }else{
            price_high = "不限";
        }
        canvas.drawText(price_high + "", (int) mOffSetHigh,
                mThumbMarginTop-lowScrollTextBg.getIntrinsicHeight()/2+5, paint);

        if (mBarChangeListener != null) {
                mBarChangeListener.onProgressChanged(this, progressLow, progressHigh);
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //按下的时候
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            if(mBarChangeListener!=null){
                mBarChangeListener.onProgressBefore();
            }
            mFlag = getAreaFlag(event);
            if(mFlag == CLICK_ON_LOW){
                mThumbLow.setState(STATE_PRESSED);
            }else if(mFlag == CLICK_ON_HIGH){
                mThumbHigh.setState(STATE_NORMAL);
            }else if(mFlag == CLICK_IN_LOW_AREA){
                mThumbHigh.setState(STATE_PRESSED);
                //如果点击0- mThumbWidth/2的坐标
                if(event.getX() <0 || event.getX() <= mThumbWidth/2){
                    mOffSetLow = mThumbWidth/2;
                }else if(event.getX() > mScrollBarWidth - mThumbWidth/2){
                        mOffSetLow = mThumbWidth/2 + mDistance;
                }else{
                    mOffSetLow = formatDouble(event.getX());
                }
            }else if(mFlag == CLICK_IN_HIGH_AREA){
                mThumbHigh.setState(STATE_PRESSED);
                if(event.getX() >= mScrollBarWidth - mThumbWidth/2) {
                    mOffSetHigh = mDistance + mThumbWidth/2;
                } else {
                    mOffSetHigh = formatDouble(event.getX());
                }
            }
            refresh();
        }else if(event.getAction() == MotionEvent.ACTION_MOVE){
            if (mFlag == CLICK_ON_LOW) {
                if (event.getX() < 0 || event.getX() <= mThumbWidth/2) {
                    mOffSetLow = mThumbWidth/2;
                } else if (event.getX() >= mScrollBarWidth - mThumbWidth/2) {
                    mOffSetLow = mThumbWidth/2 + mDistance;
                    mOffSetHigh = mOffSetLow;
                } else {
                    mOffSetLow = formatDouble(event.getX());
                    if (mOffSetHigh - mOffSetLow <= 0) {
                        mOffSetHigh = (mOffSetLow <= mDistance+mThumbWidth/2) ? (mOffSetLow) : (mDistance+mThumbWidth/2);
                    }
                }
            } else if (mFlag == CLICK_ON_HIGH) {
                if (event.getX() <  mThumbWidth/2) {
                    mOffSetHigh = mThumbWidth/2;
                    mOffSetLow = mThumbWidth/2;
                } else if (event.getX() > mScrollBarWidth - mThumbWidth/2) {
                    mOffSetHigh = mThumbWidth/2 + mDistance;
                } else {
                    mOffSetHigh = formatDouble(event.getX());
                    if (mOffSetHigh - mOffSetLow <= 0) {
                        mOffSetLow = (mOffSetHigh >= mThumbWidth/2) ? (mOffSetHigh) : mThumbWidth/2;
                    }
                }
            }
            //设置进度条
            refresh();
        }else if(event.getAction() == MotionEvent.ACTION_UP){
            mThumbLow.setState(STATE_NORMAL);
            mThumbHigh.setState(STATE_NORMAL);
            if (mBarChangeListener != null) {
                mBarChangeListener.onProgressAfter();
            }
        }
        return true;
    }

    public int getAreaFlag(MotionEvent e) {

        int top = mThumbMarginTop;
        int bottom = mThumbHeight + mThumbMarginTop;
        if (e.getY() >= top && e.getY() <= bottom && e.getX() >= (mOffSetLow - mThumbWidth / 2) && e.getX() <= mOffSetLow + mThumbWidth / 2) {
            return CLICK_ON_LOW;
        } else if (e.getY() >= top && e.getY() <= bottom && e.getX() >= (mOffSetHigh - mThumbWidth / 2) && e.getX() <= (mOffSetHigh + mThumbWidth / 2)) {
            return CLICK_ON_HIGH;
        } else if (e.getY() >= top
                && e.getY() <= bottom
                && ((e.getX() >= 0 && e.getX() < (mOffSetLow - mThumbWidth / 2)) || ((e.getX() > (mOffSetLow + mThumbWidth / 2))
                && e.getX() <= ((double) mOffSetHigh + mOffSetLow) / 2))) {
            return CLICK_IN_LOW_AREA;
        } else if (e.getY() >= top
                && e.getY() <= bottom
                && (((e.getX() > ((double) mOffSetHigh + mOffSetLow) / 2) && e.getX() < (mOffSetHigh - mThumbWidth / 2)) || (e
                .getX() > (mOffSetHigh + mThumbWidth/2) && e.getX() <= mScrollBarWidth))) {
            return CLICK_IN_HIGH_AREA;
        } else if (!(e.getX() >= 0 && e.getX() <= mScrollBarWidth && e.getY() >= top && e.getY() <= bottom)) {
            return CLICK_OUT_AREA;
        } else {
            return CLICK_INVAILD;
        }
    }

    private void refresh() {
        invalidate();
    }
    public void setProgressLow(double  progressLow) {
        this.defaultScreenLow = progressLow;
        mOffSetLow = formatDouble(progressLow / 100 * (mDistance ))+ mThumbWidth / 2;
        refresh();
    }

    public void setProgressHigh(double  progressHigh) {
        this.defaultScreenHigh = progressHigh;
        mOffSetHigh = formatDouble(progressHigh / 100 * (mDistance)) + mThumbWidth / 2;
        refresh();
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener mListener) {
        this.mBarChangeListener = mListener;
    }
    public interface OnSeekBarChangeListener {
        //滑动前
        public void onProgressBefore();

        //滑动时
        public void onProgressChanged(HScrollPriceSeekBar seekBar, double progressLow,
                                      double progressHigh);

        //滑动后
        public void onProgressAfter();
    }
}
