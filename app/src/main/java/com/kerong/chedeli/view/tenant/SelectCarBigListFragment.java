package com.kerong.chedeli.view.tenant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.adapter.BigCarListAdapter;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.data.modal.CarSimpleModal;
import com.kerong.chedeli.view.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 选车界面--每一个仅有一个试图
 * Created by LiYaoHua on 2015/9/22.
 */
public class SelectCarBigListFragment
        extends BaseFragment {

    private List<CarSimpleModal> itemList = new ArrayList<CarSimpleModal>();
    @Bind(R.id.list_view)
    ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selectcarbiglist, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    protected void init(){
        itemList = TestData.getTestCarSimpleList();
        BigCarListAdapter bigCarListAdapter = new BigCarListAdapter(getActivity(),itemList);
        listView.setAdapter(bigCarListAdapter);
        bigCarListAdapter.notifyDataSetChanged();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                turnToActivity(CarDetailActivity.class);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
