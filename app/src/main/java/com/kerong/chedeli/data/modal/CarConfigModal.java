package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * 车辆配置
 * Created by LiYaoHua on 2015/10/5.
 */
public class CarConfigModal implements Parcelable {
    private String displacement;
    private String gearbox;
    private String oiltype;
    private String mileage;
    private String seat;
    private String regyear;
    private List<CarItemModal> items;
    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public String getOiltype() {
        return oiltype;
    }

    public void setOiltype(String oiltype) {
        this.oiltype = oiltype;
    }

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getRegyear() {
        return regyear;
    }

    public void setRegyear(String regyear) {
        this.regyear = regyear;
    }

    public List<CarItemModal> getItems() {
        return items;
    }

    public void setItems(List<CarItemModal> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.displacement);
        dest.writeString(this.gearbox);
        dest.writeString(this.oiltype);
        dest.writeString(this.mileage);
        dest.writeString(this.seat);
        dest.writeString(this.regyear);
        dest.writeList(this.items);
    }

    public CarConfigModal() {
    }

    protected CarConfigModal(Parcel in) {
        this.displacement = in.readString();
        this.gearbox = in.readString();
        this.oiltype = in.readString();
        this.mileage = in.readString();
        this.seat = in.readString();
        this.regyear = in.readString();
        this.items = new ArrayList<CarItemModal>();
        in.readList(this.items, List.class.getClassLoader());
    }

    public static final Parcelable.Creator<CarConfigModal> CREATOR = new Parcelable.Creator<CarConfigModal>() {
        public CarConfigModal createFromParcel(Parcel source) {
            return new CarConfigModal(source);
        }

        public CarConfigModal[] newArray(int size) {
            return new CarConfigModal[size];
        }
    };
}
