package com.kerong.chedeli.view.user;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.kerong.chedeli.R;
import com.kerong.chedeli.view.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 租车协议界面
 * Created by LiYaoHua on 2015/10/6.
 */
public class AgreementActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agreement);
        ButterKnife.bind(this);
        initToolBar(toolbar);
    }
}
