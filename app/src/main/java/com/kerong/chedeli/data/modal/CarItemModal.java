package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 配置参数对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class CarItemModal implements Parcelable {
    private String item_code;
    private String item_name;

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.item_code);
        dest.writeString(this.item_name);
    }

    public CarItemModal() {
    }

    protected CarItemModal(Parcel in) {
        this.item_code = in.readString();
        this.item_name = in.readString();
    }

    public static final Parcelable.Creator<CarItemModal> CREATOR = new Parcelable.Creator<CarItemModal>() {
        public CarItemModal createFromParcel(Parcel source) {
            return new CarItemModal(source);
        }

        public CarItemModal[] newArray(int size) {
            return new CarItemModal[size];
        }
    };
}
