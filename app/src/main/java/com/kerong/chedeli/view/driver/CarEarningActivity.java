package com.kerong.chedeli.view.driver;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.kerong.chedeli.R;
import com.kerong.chedeli.view.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 座驾收益
 * Created by LiYaoHua on 2015/9/22.
 */
public class CarEarningActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carearning);
        ButterKnife.bind(this);
        initToolBar(toolbar);
    }

    @OnClick(R.id.bt_send_car)
    public void onSendCarClick(View view){
        turnToActivityByFinish(EditCarActivity.class);
    }
}
