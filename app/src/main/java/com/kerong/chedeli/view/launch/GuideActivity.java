package com.kerong.chedeli.view.launch;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;
import com.kerong.chedeli.view.home.MainActivity;
import com.kerong.chedeli.view.home.MainNewActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 用户初次进入引导界面
 * Created by LiYaoHua on 2015/9/21.
 */
public class GuideActivity extends BaseActivity {
    @Bind(R.id.view_pager)
    ViewPager viewPager;
    List<ImageView> itemList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFullScreen();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);
        init();
    }

    private void init(){
        itemList = new ArrayList<>();
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(R.mipmap.guide01);
        itemList.add(imageView);
        ImageView imageView2 = new ImageView(this);
        imageView2.setImageResource(R.mipmap.guide02);
        itemList.add(imageView2);
        ImageView imageView3 = new ImageView(this);
       imageView3.setImageResource(R.mipmap.guide03);
        itemList.add(imageView3);
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.showHome(GuideActivity.this,true);
            }
        });
        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return itemList.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(itemList.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(itemList.get(position));
                return itemList.get(position);
            }
        };
        viewPager.setAdapter(pagerAdapter);
    }
}
