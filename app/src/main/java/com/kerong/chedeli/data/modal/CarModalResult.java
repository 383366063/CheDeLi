package com.kerong.chedeli.data.modal;

/**
 * Created by lyh on 15/10/17.
 */
public class CarModalResult {
    private int status;
    private String message;
    private CarInfoModal results;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CarInfoModal getResults() {
        return results;
    }

    public void setResults(CarInfoModal results) {
        this.results = results;
    }
}
