package com.kerong.chedeli.view.driver;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.view.BaseActivity;

import net.tsz.afinal.FinalBitmap;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 发布座驾
 * Created by LiYaoHua on 2015/9/22.
 */
public class SendCarActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.iv_car_photo)
    ImageView iv_car_photo;
    FinalBitmap finalBitmap = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendcar);
        ButterKnife.bind(this);
        initToolBar(toolbar);
        finalBitmap = FinalBitmap.create(this);
        init();
    }

    private void init(){
        finalBitmap.display(iv_car_photo, TestData.getTestImageUrl());
    }

    @OnClick({R.id.bt_send_car,R.id.car_item})
    public void onEditCarClick(View view){
        turnToActivity(EditCarActivity.class);
    }
}
