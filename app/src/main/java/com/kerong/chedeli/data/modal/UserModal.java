package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

import net.tsz.afinal.annotation.sqlite.Id;
import net.tsz.afinal.annotation.sqlite.Table;

/**
 * Created by LiYaoHua on 2015/9/21.
 */
@Table(name = "cdl_user")
public class UserModal implements Parcelable {
    @Id(column = "dbid")
    private int dbid;
    private String usrmobile;//用户手机号
    private String usrpass;//用户密码
    private String usrname;//用户姓名
    private String invitecode;//邀请手机号
    private String phoneos;//手机系统
    private String usrdrivcemodel;//手机型号
    private String regdate;//注册日期
    private int vip_level;//用户等级
    private String vip_card;//会员卡号
    private String sessionid;//临时sessionid

    public int getDbid() {
        return dbid;
    }

    public void setDbid(int dbid) {
        this.dbid = dbid;
    }

    public String getUsrmobile() {
        return usrmobile;
    }

    public void setUsrmobile(String usrmobile) {
        this.usrmobile = usrmobile;
    }

    public String getUsrpass() {
        return usrpass;
    }

    public void setUsrpass(String usrpass) {
        this.usrpass = usrpass;
    }

    public String getUsrname() {
        return usrname;
    }

    public void setUsrname(String usrname) {
        this.usrname = usrname;
    }

    public String getInvitecode() {
        return invitecode;
    }

    public void setInvitecode(String invitecode) {
        this.invitecode = invitecode;
    }

    public String getPhoneos() {
        return "106";//默认106是android机型
    }

    public void setPhoneos(String phoneos) {
        this.phoneos = "106";
    }

    public String getUsrdrivcemodel() {
        return usrdrivcemodel;
    }

    public void setUsrdrivcemodel(String usrdrivcemodel) {
        this.usrdrivcemodel = usrdrivcemodel;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }

    public int getVip_level() {
        return vip_level;
    }

    public void setVip_level(int vip_level) {
        this.vip_level = vip_level;
    }

    public String getVip_card() {
        return vip_card;
    }

    public void setVip_card(String vip_card) {
        this.vip_card = vip_card;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.dbid);
        dest.writeString(this.usrmobile);
        dest.writeString(this.usrpass);
        dest.writeString(this.usrname);
        dest.writeString(this.invitecode);
        dest.writeString(this.phoneos);
        dest.writeString(this.usrdrivcemodel);
        dest.writeString(this.regdate);
        dest.writeInt(this.vip_level);
        dest.writeString(this.vip_card);
        dest.writeString(this.sessionid);
    }

    public UserModal() {
    }

    protected UserModal(Parcel in) {
        this.dbid = in.readInt();
        this.usrmobile = in.readString();
        this.usrpass = in.readString();
        this.usrname = in.readString();
        this.invitecode = in.readString();
        this.phoneos = in.readString();
        this.usrdrivcemodel = in.readString();
        this.regdate = in.readString();
        this.vip_level = in.readInt();
        this.vip_card = in.readString();
        this.sessionid = in.readString();
    }

    public static final Parcelable.Creator<UserModal> CREATOR = new Parcelable.Creator<UserModal>() {
        public UserModal createFromParcel(Parcel source) {
            return new UserModal(source);
        }

        public UserModal[] newArray(int size) {
            return new UserModal[size];
        }
    };
}
