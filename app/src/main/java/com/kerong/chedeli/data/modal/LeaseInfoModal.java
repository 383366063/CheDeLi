package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 租车信息对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class LeaseInfoModal implements Parcelable {
    private String taketime;
    private String backtime;
    private String leaseprice;
    private String baoxianprice;
    private String outkmprice;

    public String getTaketime() {
        return taketime;
    }

    public void setTaketime(String taketime) {
        this.taketime = taketime;
    }

    public String getBacktime() {
        return backtime;
    }

    public void setBacktime(String backtime) {
        this.backtime = backtime;
    }

    public String getLeaseprice() {
        return leaseprice;
    }

    public void setLeaseprice(String leaseprice) {
        this.leaseprice = leaseprice;
    }

    public String getBaoxianprice() {
        return baoxianprice;
    }

    public void setBaoxianprice(String baoxianprice) {
        this.baoxianprice = baoxianprice;
    }

    public String getOutkmprice() {
        return outkmprice;
    }

    public void setOutkmprice(String outkmprice) {
        this.outkmprice = outkmprice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.taketime);
        dest.writeString(this.backtime);
        dest.writeString(this.leaseprice);
        dest.writeString(this.baoxianprice);
        dest.writeString(this.outkmprice);
    }

    public LeaseInfoModal() {
    }

    protected LeaseInfoModal(Parcel in) {
        this.taketime = in.readString();
        this.backtime = in.readString();
        this.leaseprice = in.readString();
        this.baoxianprice = in.readString();
        this.outkmprice = in.readString();
    }

    public static final Parcelable.Creator<LeaseInfoModal> CREATOR = new Parcelable.Creator<LeaseInfoModal>() {
        public LeaseInfoModal createFromParcel(Parcel source) {
            return new LeaseInfoModal(source);
        }

        public LeaseInfoModal[] newArray(int size) {
            return new LeaseInfoModal[size];
        }
    };
}
