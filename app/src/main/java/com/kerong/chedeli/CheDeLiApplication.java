package com.kerong.chedeli;

import android.app.Application;
import android.widget.LinearLayout;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.SDKInitializer;
import com.kerong.chedeli.data.modal.UserModal;
import com.orhanobut.logger.Logger;

import net.tsz.afinal.FinalDb;
import net.tsz.afinal.FinalHttp;

import org.apache.http.client.CookieStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 全局Application
 * Created by LiYaoHua on 2015/9/21.
 */
public class CheDeLiApplication extends Application {
    private static CheDeLiApplication application;
    //是否是第一次打开APP
    public static final String PRE_FIRST_OPEN = "firstopen";
    //登录状态
    public static final String PRE_LOGIN_STATUS = "loginstatus";

    //事件类型
    public static final String EVENT_TYPE_LOGIN_SUCCESS = "login_success";
    public static final String EVENT_TYPE_LOGIN_FAILED = "login_failed";
    public static final Integer PAGE_SIZE = 10;
    public static Map<String,String> CODE_MAP = new HashMap<String,String>();
    public static Map<String,String> STATUS_MAP = new HashMap<String,String>();
    public static BDLocation  location= null;
    public static CookieStore cookieStore = null;
    public CheDeLiApplication(){
        application = this;
    }

    /**
     * 获得Application的单例
     * @return
     */
    public static synchronized CheDeLiApplication getInstance(){
        if( application == null){
            application = new CheDeLiApplication();
        }
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.init("CheDeLi");
        //初始化数据
        CODE_MAP.put("0","正常");
        CODE_MAP.put("1", "数据库执行错误");
        CODE_MAP.put("2","请求参数非法或不正确");
        CODE_MAP.put("3","Session不存在或失效");
        CODE_MAP.put("4","手机验证码校验失效");
        CODE_MAP.put("5","IP或手机号被限");
        CODE_MAP.put("6","非手机客户端发起的请求");
        STATUS_MAP.put("1","预订中");
        STATUS_MAP.put("2","预订成功");
        STATUS_MAP.put("3","已完成");
        STATUS_MAP.put("4","网点撤销");
        STATUS_MAP.put("5","用户撤销");

        SDKInitializer.initialize(this);

    }

    public static UserModal getUserMoal(){
        Logger.d("查询缓存的的用户信息");
        FinalDb finalDb = FinalDb.create(getInstance(),"chedeli");
        List<UserModal> userList = finalDb.findAll(UserModal.class);
        if(null!=userList && userList.size()>0){
            return userList.get(0);
        }else{
            return null;
        }
    }

    public static boolean saveUserModal(UserModal userModal){
        FinalDb finalDb = FinalDb.create(getInstance(),"chedeli");
        finalDb.deleteAll(UserModal.class);
        try {
            userModal.setDbid(1);
            finalDb.save(userModal);
            return true;
        }catch (Exception e){
            return false;
        }

    }

    /*
 * 毫秒转化时分秒毫秒
 */
    public static String formatTime(long ms) {

        Integer hh =  60;
        Integer dd = hh * 24;

        Long day = ms / dd;
        Long hour = (ms - day * dd) / hh;
        Long minute = ms - day * dd - hour * hh;


        StringBuffer sb = new StringBuffer();
        if(day > 0) {
            sb.append(day+"天");
        }
        if(hour > 0) {
            sb.append(hour+"小时");
        }
        if(minute > 0) {
            sb.append(minute+"分");
        }

        return sb.toString();
    }

}
