package com.kerong.chedeli.adapter;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.data.modal.OrderModal;

import junit.framework.Test;

import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * Created by LiYaoHua on 2015/10/7.
 */
public class OrderListAdapter extends BaseAdapter{
    private List<OrderModal> itemList;
    private Context context;
    FinalBitmap finalBitmap = null;
    public OrderListAdapter(Context context,List<OrderModal> itemList){
        this.context = context;
        this.itemList = itemList;
        finalBitmap = FinalBitmap.create(context);
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_order,null);
            viewHolder = new ViewHolder();
            viewHolder.iv_car_photo = (ImageView) convertView.findViewById(R.id.iv_car_photo);
            viewHolder.tv_car_name = (TextView) convertView.findViewById(R.id.tv_car_name);
            viewHolder.tv_vin = (TextView) convertView.findViewById(R.id.tv_vin);
            viewHolder.tv_gearbox = (TextView) convertView.findViewById(R.id.tv_gearbox);
            viewHolder.tv_car_address = (TextView) convertView.findViewById(R.id.tv_car_address);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        OrderModal orderModal = itemList.get(position);
        finalBitmap.display(viewHolder.iv_car_photo,orderModal.getCarinfo().getCoverphoto());
        viewHolder.tv_car_name.setText(orderModal.getCarinfo().getBrand_name());
        viewHolder.tv_vin.setText(orderModal.getCarinfo().getVin());
        viewHolder.tv_gearbox.setText(orderModal.getCarinfo().getGearbox().equals("1")?"自动":"手动");
        viewHolder.tv_car_address.setText(orderModal.getCarinfo().getAddress());
        return convertView;
    }

    static class ViewHolder{
        ImageView iv_car_photo;
        TextView tv_car_name;
        TextView tv_vin;
        TextView tv_gearbox;
        TextView tv_car_address;
    }
}
