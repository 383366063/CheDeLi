package com.kerong.chedeli;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.kerong.chedeli.data.modal.UserModal;
import com.kerong.chedeli.data.modal.UserResult;
import com.kerong.chedeli.event.LoginEvent;
import com.kerong.chedeli.utils.PreferencesUtils;
import com.kerong.chedeli.view.UIHelper;
import com.orhanobut.logger.Logger;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.apache.http.impl.client.DefaultHttpClient;

import de.greenrobot.event.EventBus;

/**
 * Created by LiYaoHua on 2015/9/21.
 */
public class AutoLoginService extends Service {

    private static final String TAG = "AutoLoginService";

    public AutoLoginService() {
        Logger.d("调用自动登录服务");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onHandleIntent();
        return super.onStartCommand(intent, flags, startId);
    }


    protected void onHandleIntent() {

        //执行登录操作然后发送广播
        final UserModal userModal = CheDeLiApplication.getUserMoal();
        PreferencesUtils.putBoolean(this,CheDeLiApplication.PRE_LOGIN_STATUS,false);
        if(null == userModal){//如果没有登录直接发布事件

        }else{
            AjaxParams params = new AjaxParams();
            params.put("usrmobile", userModal.getUsrmobile());
            params.put("usrpass", userModal.getUsrpass());
            params.put("phoneos", "2");
            String url = "http://cdlmapi.0371e.cn/v1/login";
            final FinalHttp finalHttp = new FinalHttp();
            finalHttp.get(url, params, new AjaxCallBack<String>() {
                @Override
                public void onSuccess(String s) {
                    try {
                        UserResult userResult = new Gson().fromJson(s,UserResult.class);
                        if(userResult.getStatus() == 0){
                            DefaultHttpClient httpClient = (DefaultHttpClient) finalHttp.getHttpClient();
                            CheDeLiApplication.cookieStore = httpClient.getCookieStore();
                            UserModal userModals = userResult.getResults();
                            userModals.setUsrmobile(userModal.getUsrmobile());
                            userModals.setUsrpass(userModal.getUsrpass());
                            CheDeLiApplication.saveUserModal(userModals);
                            Logger.i("----服务后台自动登陆成功----");
                            PreferencesUtils.putBoolean(AutoLoginService.this,
                                    CheDeLiApplication.PRE_LOGIN_STATUS, true);
                            EventBus.getDefault().post("loginsuccess");
                        }else{
                            Logger.i("----服务后台自动登陆失败----");
                        }
                    }catch (Exception e){
                        Logger.i("----服务后台自动登陆失败----");
                    }
                }

                @Override
                public void onFailure(Throwable t, int errorNo, String strMsg) {
                    Logger.i("----服务后台自动登陆失败----");
                }
            });
        }

    }
}
