package com.kerong.chedeli.view.tenant;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.kerong.chedeli.CheDeLiApplication;
import com.kerong.chedeli.R;
import com.kerong.chedeli.data.modal.CarConfigModal;
import com.kerong.chedeli.data.modal.CarInfoModal;
import com.kerong.chedeli.data.modal.CarItemModal;
import com.kerong.chedeli.data.modal.CarModalResult;
import com.kerong.chedeli.data.modal.CarSimpleModal;
import com.kerong.chedeli.data.modal.PhotoModal;
import com.kerong.chedeli.data.modal.PriceModal;

import com.kerong.chedeli.utils.PreferencesUtils;

import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;
import com.kerong.chedeli.view.user.LoginActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import net.tsz.afinal.FinalBitmap;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

/**
 * 详情界面
 * Created by LiYaoHua on 2015/10/6.
 */
public class CarDetailActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.tv_car_name)
    TextView tv_car_name;
    @Bind(R.id.vp_car_photo)
    ViewPager vp_car_photo;

    @Bind(R.id.tv_price)
    TextView tv_price;
    @Bind(R.id.tv_carname2)
    TextView tv_carname2;
    @Bind(R.id.tv_carnumber)
    TextView tv_carnumber;
    @Bind(R.id.tv_displacement)
    TextView tv_displacement;
    @Bind(R.id.tv_cartype)
    TextView tv_cartype;
    @Bind(R.id.tv_biansuxiang)
    TextView tv_biansuxiang;
    @Bind(R.id.tv_seats)
    TextView tv_seats;
    //    @Bind(R.id.tv_paizhaotype)
//    TextView tv_paizhaotype;
    @Bind(R.id.tv_dinstance)
    TextView tv_dinstance;
    //    @Bind(R.id.tv_jiaochefangfa)
//    TextView tv_jiaochefangfa;
    @Bind(R.id.tv_nianfen)
    TextView tv_nianfen;
    @Bind(R.id.tv_desc)
    TextView tv_desc;
    @Bind(R.id.tv_car_address)
    TextView tv_car_address;
    @Bind(R.id.layout_item1)
    LinearLayout layout_item1;
    @Bind(R.id.tv_item1_item1)
    TextView tv_item1_item1;
    @Bind(R.id.tv_item1_item2)
    TextView tv_item1_item2;
    @Bind(R.id.layout_item2)
    LinearLayout layout_item2;
    @Bind(R.id.tv_item2_item1)
    TextView tv_item2_item1;
    @Bind(R.id.tv_item2_item2)
    TextView tv_item2_item2;
    @Bind(R.id.layout_item3)
    LinearLayout layout_item3;
    @Bind(R.id.tv_item3_item1)
    TextView tv_item3_item1;
    @Bind(R.id.tv_item3_item2)
    TextView tv_item3_item2;
    @Bind(R.id.step_desc)
    TextView step_desc;

    @Bind(R.id.iv_static_map)
    ImageView iv_static_map;
    @Bind(R.id.bt_order_submit)
    Button bt_order_submit;
    FinalBitmap finalBitmap = null;
    List<View> viewList = null;
    CarSimpleModal carSimple;
    CarInfoModal carInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardetail);
        ButterKnife.bind(this);
        initToolBar(toolbar);
        finalBitmap  = FinalBitmap.create(this);
        carSimple = this.getIntent().getExtras().getParcelable("carModal");
        loadData();
    }
    public void loadData(){
        AjaxParams params = new AjaxParams();
        params.put("carid", carSimple.getCarid());
        String url = "http://cdlmapi.0371e.cn/v1/cardetail";
        FinalHttp finalHttp = new FinalHttp();
        finalHttp.get(url, params, new AjaxCallBack<String>() {
            @Override
            public void onSuccess(String s) {
                try {
                    CarModalResult carModalResult = new Gson().fromJson(s,CarModalResult.class);
                    if(carModalResult.getStatus()==0){
                        carInfo = carModalResult.getResults();
                        showView();
                    }else{
                        UIHelper.showMessage(carModalResult.getMessage());
                    }
                }catch (Exception e){

                }

            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                UIHelper.showMessage(strMsg);
            }
        });
    }

    private void showView(){
        viewList = new ArrayList<>();
        for(PhotoModal photo: carInfo.getPhotos()){
            View view = getLayoutInflater().inflate(R.layout.item_car_photo,null);
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_car_photo);
            finalBitmap.display(imageView,photo.getPhoto_filename());
            viewList.add(view);
        }
        PagerAdapter pagerAdapter = new PagerAdapter() {
            @Override
            public int getCount() {
                return viewList.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView(viewList.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewList.get(position));
                return viewList.get(position);
            }
        };
        step_desc.setText("1/"+viewList.size());
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mScreenWidth = dm.widthPixels;// 获取屏幕分辨率宽度
        if(mScreenWidth>1024) mScreenWidth =1024;//不能超过1024
        String mapUrl = "http://api.map.baidu.com/staticimage?center="+carInfo.getLocation().getLng()+","+carInfo.getLocation().getLat()+"&width="+mScreenWidth+"&height=300&zoom=18" +
                "&markers="+carInfo.getLocation().getLng()+","+carInfo.getLocation().getLat()+"&markerStyles=l,,red";

        vp_car_photo.setAdapter(pagerAdapter);
        vp_car_photo.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                step_desc.setText((position+1)+"/"+viewList.size());
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        finalBitmap.display(iv_static_map, mapUrl);
        tv_price.setText(getPriceStr(carInfo.getPrices()));
        showItems(carInfo.getCar_config().getItems());


        tv_car_name.setText(carInfo.getBrand_name()+" "+carInfo.getModel_name());
        tv_carname2.setText(carInfo.getBrand_name()+" "+carInfo.getModel_name());
        tv_carnumber.setText(carInfo.getVin());
        CarConfigModal config = carInfo.getCar_config();
        tv_displacement.setText(config.getDisplacement());
        tv_cartype.setText(config.getOiltype());
        tv_biansuxiang.setText(config.getGearbox().equals("1")?"自动":"手动");
        tv_seats.setText(config.getSeat()+"座");
        tv_dinstance.setText(config.getMileage()+"公里");
        tv_nianfen.setText(config.getRegyear()+"年");
        tv_desc.setText(carInfo.getDescription());
        tv_car_address.setText(carInfo.getAddress());

    }

    @OnClick({R.id.bt_order_submit,R.id.tv_use_car})
    public void orderSubmit(View button){
        if(!PreferencesUtils.getBoolean(this,CheDeLiApplication.PRE_LOGIN_STATUS,false)
                || null == CheDeLiApplication.getUserMoal()){
            turnToActivity(LoginActivity.class);
            return;
        }
        Intent intent = new Intent();
        intent.putExtra("carSimple", carSimple);
        intent.putExtra("carid", carInfo.getCarid());
        intent.putExtra("usrmobile", CheDeLiApplication.getUserMoal().getUsrmobile());
        turnToActivity(GetOrderActivity.class,intent);
    }

    public String getPriceStr(PriceModal modeal){
//    	 private String minute;
//    	    private String hour;
//    	    private String day;
//    	    private String month;
//    	    private String kmpriceofday;
        StringBuffer sb = new StringBuffer();
        sb.append("￥"+modeal.getMinute()+"元/分钟 ");
       // sb.append(modeal.getHour() + "元/时;");
        sb.append(" ￥"+modeal.getDay()+"元/天");
       // sb.append(modeal.getMonth()+"元/月");
        return sb.toString();
    }

    public void showItems(List<CarItemModal> items){
        int size = items.size();
        if(size == 0){
            layout_item1.setVisibility(View.GONE);
            layout_item2.setVisibility(View.GONE);
            layout_item3.setVisibility(View.GONE);
        }else if(size == 1){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setVisibility(View.GONE);
            layout_item2.setVisibility(View.GONE);
            layout_item3.setVisibility(View.GONE);
        }else if(size == 2){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            layout_item2.setVisibility(View.GONE);
            layout_item3.setVisibility(View.GONE);
        }else if(size == 3){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            tv_item2_item1.setText(items.get(2).getItem_name());
            tv_item2_item2.setVisibility(View.GONE);
            layout_item3.setVisibility(View.GONE);
        }else if(size == 4){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            tv_item2_item1.setText(items.get(2).getItem_name());
            tv_item2_item2.setText(items.get(3).getItem_name());
            layout_item3.setVisibility(View.GONE);
        }else if(size == 5){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            tv_item2_item1.setText(items.get(2).getItem_name());
            tv_item2_item2.setText(items.get(3).getItem_name());
            tv_item3_item1.setText(items.get(4).getItem_name());
            tv_item3_item2.setVisibility(View.GONE);
        }else if(size == 6){
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            tv_item2_item1.setText(items.get(2).getItem_name());
            tv_item2_item2.setText(items.get(3).getItem_name());
            tv_item3_item1.setText(items.get(4).getItem_name());
            tv_item3_item2.setText(items.get(5).getItem_name());
        }else{
            tv_item1_item1.setText(items.get(0).getItem_name());
            tv_item1_item2.setText(items.get(1).getItem_name());
            tv_item2_item1.setText(items.get(2).getItem_name());
            tv_item2_item2.setText(items.get(3).getItem_name());
            tv_item3_item1.setText(items.get(4).getItem_name());
            tv_item3_item2.setText(items.get(5).getItem_name());
        }
    }



}
