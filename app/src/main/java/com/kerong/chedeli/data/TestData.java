package com.kerong.chedeli.data;

import com.kerong.chedeli.data.modal.CarSimpleModal;
import com.kerong.chedeli.data.modal.PriceModal;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试数据
 * Created by LiYaoHua on 2015/10/5.
 */
public class TestData {

    public static List<CarSimpleModal> getTestCarSimpleList(){
        List<CarSimpleModal> itemList = new ArrayList<CarSimpleModal>();
        for(int i=0 ; i <10 ;i++){
            CarSimpleModal carSimpleModal = new CarSimpleModal();
            carSimpleModal.setCarid("10" + i);
            carSimpleModal.setAddress("河南省郑州市二七区大学路" + i + "号");
            carSimpleModal.setBrand_name("特斯拉");
            carSimpleModal.setModel_name("MODELS");
            carSimpleModal.setVin("豫A");
            carSimpleModal.setGearbox((i % 2 + 1) + "");
            carSimpleModal.setDistance(100 * i + "");
            carSimpleModal.setCoverphoto("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510563242.jpg");
            PriceModal priceModal = new PriceModal();
            priceModal.setDay("120");
            carSimpleModal.setPrices(priceModal);
            carSimpleModal.setDisplacement("1.8T");
            itemList.add(carSimpleModal);
        }
        return  itemList;
    }

    public static List<String> getPhotoUrl(){
        List<String> itemList = new ArrayList<>();
        itemList.add("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510571146.jpg");
        itemList.add("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510563242.jpg");
        itemList.add("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510580261.jpg");
        itemList.add("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510587851.jpg");
        itemList.add("http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510595292.jpg");
        return itemList;
    }

    public static String getTestImageUrl(){
        return "http://at-images.oss-cn-hangzhou.aliyuncs.com/car/15/05/48146808/1431510571146.jpg";
    }
}
