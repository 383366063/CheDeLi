package com.kerong.chedeli.view.tenant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.SupportMapFragment;
import com.kerong.chedeli.R;
import com.kerong.chedeli.view.BaseFragment;

import org.w3c.dom.Text;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 选车界面地图试图
 * Created by LiYaoHua on 2015/9/22.
 */
public class SelectCarMapFragment extends BaseFragment {

    @Bind(R.id.map)
    MapView mapView;
    BaiduMap baiduMap;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragment_carmap,container,false);
        ButterKnife.bind(this, view);
        //初始化百度地图
        baiduMap = mapView.getMap();
        return view;
    }

    @Override
    protected void init() {
        super.init();
        baiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);

    }
}
