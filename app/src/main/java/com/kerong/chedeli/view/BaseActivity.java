package com.kerong.chedeli.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.kerong.chedeli.R;
import com.kerong.chedeli.utils.AppManager;

/**
 * Activity的基类
 * Created by LiYaoHua on 2015/9/21.
 */
public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().finishActivity(this);
    }

    /**
     * 初始化ActionBar 并且带返回功能
     * @param toolbar
     */
    protected void initToolBar(Toolbar toolbar){
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_left);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    /**
     * 初始化ActionBar 并且带返回功能
     * @param toolbar
     */
    protected void initToolBar(Toolbar toolbar,boolean userup){
        toolbar.setNavigationIcon(R.mipmap.ic_arrow_left);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(userup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(userup);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    /**
     * 设置全屏显示
     */
    protected void setFullScreen(){
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void turnToActivity(Class<? extends Activity> activity){
        Intent intent = new Intent(this,activity);
        startActivity(intent);
    }

    protected void turnToActivityByFinish(Class<? extends Activity> activity){
        Intent intent = new Intent();
        intent.setClass(this,activity);
        startActivity(intent);
        finish();
    }

    protected  void turnToActivity(Class<? extends Activity> activity,Intent intent){
        intent.setClass(this, activity);
        startActivity(intent);
    }
    protected void turnToActivityByFinish(Class<? extends Activity> activity,Intent intent){
        intent.setClass(this,activity);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
