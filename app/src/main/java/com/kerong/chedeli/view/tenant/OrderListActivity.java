package com.kerong.chedeli.view.tenant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.kerong.chedeli.CheDeLiApplication;
import com.kerong.chedeli.R;
import com.kerong.chedeli.adapter.OrderListAdapter;
import com.kerong.chedeli.data.modal.OrderModal;
import com.kerong.chedeli.data.modal.OrderResultModal;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 订单列表
 * Created by LiYaoHua on 2015/9/22.
 */
public class OrderListActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.list_view)
    ListView listView;
    @Bind(R.id.no_date_layout)
    RelativeLayout no_date_layout;
    private List<OrderModal> itemList;
    private OrderListAdapter orderListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderlist);
        ButterKnife.bind(this);
        initToolBar(toolbar);
        init();
    }

    private void init(){
        itemList = new ArrayList<>();

        orderListAdapter = new OrderListAdapter(this,itemList);
        listView.setAdapter(orderListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                turnToActivity(OrderInfoActivity.class);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String orderId = itemList.get(position).getOrderid();
                Intent intent = new Intent();
                intent.putExtra("orderid",orderId);
                turnToActivity(OrderInfoActivity.class,intent);
            }
        });
        initOrderData();
    }

    private void initOrderData(){
        String url = "http://cdlmapi.0371e.cn/v1/ordersearch";
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("usrmobile", CheDeLiApplication.getUserMoal().getUsrmobile());
        ajaxParams.put("starttime","");
        ajaxParams.put("endtime","");
        ajaxParams.put("orderstate","-1");
        ajaxParams.put("pagesize","100");
        ajaxParams.put("pagenum","1");
        FinalHttp finalHttp = new FinalHttp();
        finalHttp.configCookieStore(CheDeLiApplication.cookieStore);
        finalHttp.get(url, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onSuccess(String s) {
                try {
                    OrderResultModal orderResultModal = new Gson().fromJson(s,OrderResultModal.class);
                    if(orderResultModal.getStatus() == 0){
                        itemList.clear();
                        itemList.addAll(orderResultModal.getResults());
                        orderListAdapter.notifyDataSetChanged();
                        if(itemList.size()==0){
                            no_date_layout.setVisibility(View.VISIBLE);
                        }
                    }
                }catch (Exception e){
                    UIHelper.showMessage("数据加载失败");
                }
            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                UIHelper.showMessage("数据加载失败");
            }
        });

    }
}
