package com.kerong.chedeli.view.tenant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.GridView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.adapter.BigCarListAdapter;
import com.kerong.chedeli.adapter.CarListAdapter;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.data.modal.CarSimpleModal;
import com.kerong.chedeli.view.BaseFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * 选车界面--瀑布流试图
 * Created by LiYaoHua on 2015/9/22.
 */
public class SelectCarListFragment extends BaseFragment {

    @Bind(R.id.grid_view)
    GridView gridView;
    List<CarSimpleModal> itemList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selectcarlist,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    protected void init() {
        itemList = TestData.getTestCarSimpleList();
        CarListAdapter bigCarListAdapter = new CarListAdapter(getActivity(),itemList);
        gridView.setAdapter(bigCarListAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                turnToActivity(CarDetailActivity.class);
            }
        });
    }
}
