package com.kerong.chedeli.data.modal;

import java.util.List;

/**
 * Created by lyh on 15/10/17.
 */
public class OrderResultModal {
    private int status;
    private String message;
    private List<OrderModal> results;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrderModal> getResults() {
        return results;
    }

    public void setResults(List<OrderModal> results) {
        this.results = results;
    }
}
