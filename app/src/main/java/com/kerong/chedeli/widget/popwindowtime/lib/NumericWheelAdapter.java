package com.kerong.chedeli.widget.popwindowtime.lib;

/**
 * Numeric Wheel adapter.
 */
public class NumericWheelAdapter implements WheelAdapter {
	
	/** The default min value */
	public static final int DEFAULT_MAX_VALUE = 9;

	/** The default max value */
	private static final int DEFAULT_MIN_VALUE = 0;
	
	// Values
	private int minValue;
	private int maxValue;
	
	// format
	private String format;
	private Integer[] halfTime ;
	
	/**
	 * Default constructor
	 */
	public NumericWheelAdapter() {
		this(DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE);
	}
	
	public NumericWheelAdapter(Integer[] halfTime){
		this.halfTime = halfTime;
	}
	/**
	 * Constructor
	 * @param minValue the wheel min value
	 * @param maxValue the wheel max value
	 */
	public NumericWheelAdapter(int minValue, int maxValue) {
		this(minValue, maxValue, null);
	}

	/**
	 * Constructor
	 * @param minValue the wheel min value
	 * @param maxValue the wheel max value
	 * @param format the format string
	 */
	public NumericWheelAdapter(int minValue, int maxValue, String format) {
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.format = format;
	}

	@Override
	public String getItem(int index) {
		if(null!=halfTime){
			if (index >= 0 && index < getItemsCount()) {
				String value = Integer.toString(halfTime[index]);
				if(value.length()==1){
					return  "0"+value;
				}else{
					return  value;
				}
			}
		}else{
			if (index >= 0 && index < getItemsCount()) {
				int value = minValue + index;
				String str = Integer.toString(value);
				if(str.length()==1){
					return format != null ? String.format(format, "0"+value) : "0"+Integer.toString(value);
				}else{
					return format != null ? String.format(format, value) : Integer.toString(value);
				}
			}
		}
		return null;
	}

	@Override
	public int getItemsCount() {
		if(null!=halfTime){
			return halfTime.length;
		}
		return maxValue - minValue + 1;
	}
	
	@Override
	public int getMaximumLength() {
		int max = 0;
		int maxLen = 0;
		if(null!=halfTime){
			max = Math.max(Math.abs(halfTime[0]), Math.abs(halfTime[1]));
			maxLen = Integer.toString(max).length();
			if(maxLen==1){
				maxLen = maxLen+1;
			}
			int minValue = Math.min(Math.abs(halfTime[0]), Math.abs(halfTime[1]));
			if (minValue < 0) {
				maxLen++;
			}
		}else{
			max = Math.max(Math.abs(maxValue), Math.abs(minValue));
			maxLen = Integer.toString(max).length();
			if(maxLen==1){
				maxLen = maxLen+1;
			}
			if (minValue < 0) {
				maxLen++;
			}
		}
		return maxLen;
	}
}
