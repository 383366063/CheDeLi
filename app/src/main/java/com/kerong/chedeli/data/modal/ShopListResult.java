package com.kerong.chedeli.data.modal;

import java.util.List;

/**
 * Created by lyh on 15/10/16.
 */
public class ShopListResult {
    private int status;
    private String message;
    private List<ShopModal> results;

    public List<ShopModal> getResults() {
        return results;
    }

    public void setResults(List<ShopModal> results) {
        this.results = results;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
