package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 司机车辆信息模型
 * Created by LiYaoHua on 2015/10/5.
 */
public class DriverCarInfo implements Parcelable {
    private String carid;
    private String vin;
    private String gearbox;
    private String coverphoto;
    private String brand_name;
    private String model_name;
    private String displacement;
    private int verify_state;

    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public String getCoverphoto() {
        return coverphoto;
    }

    public void setCoverphoto(String coverphoto) {
        this.coverphoto = coverphoto;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public int getVerify_state() {
        return verify_state;
    }

    public void setVerify_state(int verify_state) {
        this.verify_state = verify_state;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.carid);
        dest.writeString(this.vin);
        dest.writeString(this.gearbox);
        dest.writeString(this.coverphoto);
        dest.writeString(this.brand_name);
        dest.writeString(this.model_name);
        dest.writeString(this.displacement);
        dest.writeInt(this.verify_state);
    }

    public DriverCarInfo() {
    }

    protected DriverCarInfo(Parcel in) {
        this.carid = in.readString();
        this.vin = in.readString();
        this.gearbox = in.readString();
        this.coverphoto = in.readString();
        this.brand_name = in.readString();
        this.model_name = in.readString();
        this.displacement = in.readString();
        this.verify_state = in.readInt();
    }

    public static final Parcelable.Creator<DriverCarInfo> CREATOR = new Parcelable.Creator<DriverCarInfo>() {
        public DriverCarInfo createFromParcel(Parcel source) {
            return new DriverCarInfo(source);
        }

        public DriverCarInfo[] newArray(int size) {
            return new DriverCarInfo[size];
        }
    };
}
