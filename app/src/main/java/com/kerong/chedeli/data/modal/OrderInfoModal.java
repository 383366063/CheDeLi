package com.kerong.chedeli.data.modal;

import android.os.Parcel;

/**
 * Created by LiYaoHua on 2015/10/5.
 */
public class OrderInfoModal extends OrderModal{
    private String shop_name;
    private String shop_phone;
    private String useStartTime;
    private String useEndTime;
    private LeaseInfoModal leaseinfo;

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    public LeaseInfoModal getLeaseinfo() {
        return leaseinfo;
    }

    public void setLeaseinfo(LeaseInfoModal leaseinfo) {
        this.leaseinfo = leaseinfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.shop_name);
        dest.writeString(this.shop_phone);
        dest.writeParcelable(this.leaseinfo, 0);
    }

    public OrderInfoModal() {
    }

    protected OrderInfoModal(Parcel in) {
        super(in);
        this.shop_name = in.readString();
        this.shop_phone = in.readString();
        this.leaseinfo = in.readParcelable(LeaseInfoModal.class.getClassLoader());
    }

    public static final Creator<OrderInfoModal> CREATOR = new Creator<OrderInfoModal>() {
        public OrderInfoModal createFromParcel(Parcel source) {
            return new OrderInfoModal(source);
        }

        public OrderInfoModal[] newArray(int size) {
            return new OrderInfoModal[size];
        }
    };

    public static Creator<OrderInfoModal> getCREATOR() {
        return CREATOR;
    }

    public String getUseStartTime() {
        return useStartTime;
    }

    public void setUseStartTime(String useStartTime) {
        this.useStartTime = useStartTime;
    }

    public String getUseEndTime() {
        return useEndTime;
    }

    public void setUseEndTime(String useEndTime) {
        this.useEndTime = useEndTime;
    }
}
