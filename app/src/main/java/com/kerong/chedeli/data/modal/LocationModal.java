package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 经纬度对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class LocationModal implements Parcelable {
    private String lat;
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.lat);
        dest.writeString(this.lng);
    }

    public LocationModal() {
    }

    protected LocationModal(Parcel in) {
        this.lat = in.readString();
        this.lng = in.readString();
    }

    public static final Parcelable.Creator<LocationModal> CREATOR = new Parcelable.Creator<LocationModal>() {
        public LocationModal createFromParcel(Parcel source) {
            return new LocationModal(source);
        }

        public LocationModal[] newArray(int size) {
            return new LocationModal[size];
        }
    };
}
