package com.kerong.chedeli.view.home;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.SeekBar;

import com.kerong.chedeli.R;
import com.kerong.chedeli.view.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 首页的条件检索
 * Created by LiYaoHua on 2015/9/22.
 */
public class SearchActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.bt_search)
    Button bt_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initToolBar(toolbar);
    }

    @OnClick(R.id.bt_search)
    public void onSearchClick(){
        finish();
    }
}
