package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 价格模型
 * Created by LiYaoHua on 2015/10/5.
 */
public class PriceModal implements Parcelable {
    private String minute;
    private String hour;
    private String day;
    private String month;
    private String kmpriceofday;

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getKmpriceofday() {
        return kmpriceofday;
    }

    public void setKmpriceofday(String kmpriceofday) {
        this.kmpriceofday = kmpriceofday;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.minute);
        dest.writeString(this.hour);
        dest.writeString(this.day);
        dest.writeString(this.month);
        dest.writeString(this.kmpriceofday);
    }

    public PriceModal() {
    }

    protected PriceModal(Parcel in) {
        this.minute = in.readString();
        this.hour = in.readString();
        this.day = in.readString();
        this.month = in.readString();
        this.kmpriceofday = in.readString();
    }

    public static final Parcelable.Creator<PriceModal> CREATOR = new Parcelable.Creator<PriceModal>() {
        public PriceModal createFromParcel(Parcel source) {
            return new PriceModal(source);
        }

        public PriceModal[] newArray(int size) {
            return new PriceModal[size];
        }
    };
}
