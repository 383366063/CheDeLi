package com.kerong.chedeli.view.user;

import com.google.gson.Gson;
import com.kerong.chedeli.R;
import com.kerong.chedeli.data.modal.ResultModal;
import com.kerong.chedeli.utils.PreferencesUtils;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;

import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

/**
 * 注册界面 Created by LiYaoHua on 2015/9/21.
 */
public class FindPWDActivity extends BaseActivity implements ViewFactory {
	@Bind(R.id.toolbar)
	Toolbar toolbar;
	@Bind(R.id.et_phone)
	EditText et_phone;
	@Bind(R.id.tv_send_code)
	TextSwitcher tv_send_code;
	@Bind(R.id.et_code)
	EditText et_code;
	@Bind(R.id.et_password)
	EditText et_password;
	String phoneNumber;
	String password;

	private TimeCount time;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findpwd);
		ButterKnife.bind(this);
		initToolBar(toolbar);
		init();
	}

	private void init() {
		tv_send_code.setFactory(this);
	}

	@OnClick(R.id.bt_register)
	public void onRegisterClick(View item) {
		phoneNumber = et_phone.getText().toString().trim();
		password = et_password.getText().toString().trim();
		if (TextUtils.isEmpty(phoneNumber)) {
			et_phone.setError("手机号不能为空");
			return;
		}
		if (TextUtils.isEmpty(password)) {
			et_phone.setError("密码不能为空");
			return;
		}

		AjaxParams params = new AjaxParams();
		params.put("usrmobile", phoneNumber);
		params.put("usrnewpass", password);
		params.put("smscode", "00000");
		String url = "http://cdlmapi.0371e.cn/v1/resetpass";
		FinalHttp finalHttp = new FinalHttp();
		finalHttp.get(url, params, new AjaxCallBack<String>() {
			@Override
			public void onSuccess(String s) {
				try {
					ResultModal resultModal = new Gson().fromJson(s, ResultModal.class);
					if (resultModal.getStatus() == 0) {
						UIHelper.showMessage("重置密码成功请重新登陆");
						finish();
					} else {
						UIHelper.showMessage(resultModal.getMessage());
					}
				} catch (Exception e) {

				}
			}

			@Override
			public void onFailure(Throwable t, int errorNo, String strMsg) {
				UIHelper.showMessage(strMsg);
			}
		});
	}

	@OnClick(R.id.tv_send_code)
	public void onGetCodeClick(View item) {
		phoneNumber = et_phone.getText().toString().trim();
		if (TextUtils.isEmpty(phoneNumber)) {
			et_phone.setError("手机号不能为空");
		} else {
			getYanZhengCode();
		}
	}

	private void getYanZhengCode() {
		if (time != null) {
			time.cancel();
		}
		time = new TimeCount(60000, 1000);
		time.start();
		AjaxParams params = new AjaxParams();
		params.put("usrmobile", phoneNumber);
		params.put("smstype", "102");
		FinalHttp finalHttp = new FinalHttp();
		String urls = "http://cdlmapi.0371e.cn/v1/sendsmspin";
		finalHttp.get(urls, params, new AjaxCallBack<String>() {
					@Override
					public void onSuccess(String s) {
						try {
							ResultModal resultModal = new Gson().fromJson(s, ResultModal.class);
							if (resultModal.getStatus() == 0) {
								UIHelper.showMessage("发送验证码成功");
							}
						} catch (Exception e) {
							UIHelper.showMessage("服务器出现错误。请联系管理员");
						}
					}

					@Override
					public void onFailure(Throwable t, int errorNo, String strMsg) {
						UIHelper.showMessage(strMsg);
					}
				}

		);
	}

	/* 定义一个倒计时的内部类 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
		}

		@Override
		public void onFinish() {// 计时完毕时触发
			tv_send_code.setText("重新发送");
			tv_send_code.setClickable(true);
		}

		@Override
		public void onTick(long millisUntilFinished) {// 计时过程显示
			tv_send_code.setClickable(false);
			tv_send_code.setText(millisUntilFinished / 1000 + "秒后可重新发送");
		}

	}



	@Override
	public View makeView() {
		// TODO Auto-generated method stub
		TextView t = new TextView(this);
		t.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		t.setGravity(Gravity.CENTER);
		t.setTextSize(14);
		t.setTextColor(getResources().getColor(R.color.txt_black));
		t.setText("获取验证码");
		return t;
	}

}
