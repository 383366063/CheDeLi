package com.kerong.chedeli.event;

/**
 * Created by LiYaoHua on 2015/9/21.
 */
public class LoginEvent {
    private boolean status;
    private String msg;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
