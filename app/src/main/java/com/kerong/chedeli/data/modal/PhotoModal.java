package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 车辆图片模型
 * Created by LiYaoHua on 2015/10/5.
 */
public class PhotoModal implements Parcelable {
    private String photo_filename;

    public String getPhoto_filename() {
        return photo_filename;
    }

    public void setPhoto_filename(String photo_filename) {
        this.photo_filename = photo_filename;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.photo_filename);
    }

    public PhotoModal() {
    }

    protected PhotoModal(Parcel in) {
        this.photo_filename = in.readString();
    }

    public static final Parcelable.Creator<PhotoModal> CREATOR = new Parcelable.Creator<PhotoModal>() {
        public PhotoModal createFromParcel(Parcel source) {
            return new PhotoModal(source);
        }

        public PhotoModal[] newArray(int size) {
            return new PhotoModal[size];
        }
    };
}
