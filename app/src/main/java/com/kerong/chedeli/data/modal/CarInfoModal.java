package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**车辆详细信息对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class CarInfoModal implements Parcelable {
    private String carid;
    private String vin;
    private String kmlimitofday;
    private PriceModal prices;
    private List<PhotoModal> photos;
    private String brandid;
    private String modelid;
    private String brand_name;
    private String model_name;
    private CarConfigModal car_config;
    private String description;
    private String address;
    private LocationModal location;
    private String distance;
    private String shop_id;
    private String shop_name;
    private String shop_phone;


    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getKmlimitofday() {
        return kmlimitofday;
    }

    public void setKmlimitofday(String kmlimitofday) {
        this.kmlimitofday = kmlimitofday;
    }

    public PriceModal getPrices() {
        return prices;
    }

    public void setPrices(PriceModal prices) {
        this.prices = prices;
    }

    public List<PhotoModal> getPhotos() {
        return photos;
    }

    public void setPhotos(List<PhotoModal> photos) {
        this.photos = photos;
    }

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public CarConfigModal getCar_config() {
        return car_config;
    }

    public void setCar_config(CarConfigModal car_config) {
        this.car_config = car_config;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocationModal getLocation() {
        return location;
    }

    public void setLocation(LocationModal location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.carid);
        dest.writeString(this.vin);
        dest.writeString(this.kmlimitofday);
        dest.writeParcelable(this.prices, flags);
        dest.writeList(this.photos);
        dest.writeString(this.brandid);
        dest.writeString(this.modelid);
        dest.writeString(this.brand_name);
        dest.writeString(this.model_name);
        dest.writeParcelable(this.car_config, flags);
        dest.writeString(this.description);
        dest.writeString(this.address);
        dest.writeParcelable(this.location, flags);
        dest.writeString(this.distance);
        dest.writeString(this.shop_id);
        dest.writeString(this.shop_name);
        dest.writeString(this.shop_phone);
    }

    public CarInfoModal() {
    }

    protected CarInfoModal(Parcel in) {
        this.carid = in.readString();
        this.vin = in.readString();
        this.kmlimitofday = in.readString();
        this.prices = in.readParcelable(PriceModal.class.getClassLoader());
        this.photos = new ArrayList<PhotoModal>();
        in.readList(this.photos, List.class.getClassLoader());
        this.brandid = in.readString();
        this.modelid = in.readString();
        this.brand_name = in.readString();
        this.model_name = in.readString();
        this.car_config = in.readParcelable(CarConfigModal.class.getClassLoader());
        this.description = in.readString();
        this.address = in.readString();
        this.location = in.readParcelable(LocationModal.class.getClassLoader());
        this.distance = in.readString();
        this.shop_id = in.readString();
        this.shop_name = in.readString();
        this.shop_phone = in.readString();
    }

    public static final Parcelable.Creator<CarInfoModal> CREATOR = new Parcelable.Creator<CarInfoModal>() {
        public CarInfoModal createFromParcel(Parcel source) {
            return new CarInfoModal(source);
        }

        public CarInfoModal[] newArray(int size) {
            return new CarInfoModal[size];
        }
    };
}
