package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LiYaoHua on 2015/10/5.
 */
public class OrderModal implements Parcelable {
    private String orderid;
    private String order_time;
    private String order_state;
    private String order_price;
    private String use_time;
    private OrderCarInfoModal carinfo;

    public static final Creator<OrderModal> CREATOR = new Creator<OrderModal>() {
        @Override
        public OrderModal createFromParcel(Parcel in) {
            return new OrderModal(in);
        }

        @Override
        public OrderModal[] newArray(int size) {
            return new OrderModal[size];
        }
    };

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getOrder_state() {
        return order_state;
    }

    public void setOrder_state(String order_state) {
        this.order_state = order_state;
    }

    public String getOrder_price() {
        return order_price;
    }

    public void setOrder_price(String order_price) {
        this.order_price = order_price;
    }

    public String getUse_time() {
        return use_time;
    }

    public void setUse_time(String use_time) {
        this.use_time = use_time;
    }

    public OrderCarInfoModal getCarinfo() {
        return carinfo;
    }

    public void setCarinfo(OrderCarInfoModal carinfo) {
        this.carinfo = carinfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.orderid);
        dest.writeString(this.order_time);
        dest.writeString(this.order_state);
        dest.writeString(this.order_price);
        dest.writeString(this.use_time);
        dest.writeParcelable(this.carinfo, 0);
    }

    public OrderModal() {
    }

    protected OrderModal(Parcel in) {
        this.orderid = in.readString();
        this.order_time = in.readString();
        this.order_state = in.readString();
        this.order_price = in.readString();
        this.use_time = in.readString();
        this.carinfo = in.readParcelable(OrderCarInfoModal.class.getClassLoader());
    }

}
