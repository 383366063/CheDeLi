package com.kerong.chedeli.data.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 车辆列表对象
 * Created by LiYaoHua on 2015/10/5.
 */
public class CarSimpleModal implements Parcelable {
    private String carid;
    private String vin;
    private String gearbox;
    private String stop_shopid;
    private String address;
    private LocationModal location;
    private String distance;
    private String brandid;
    private String modelid;
    private String brand_name;
    private String model_name;
    private String displacement;
    private PriceModal prices;
    private String coverphoto;

    public String getCarid() {
        return carid;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getGearbox() {
        return gearbox;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public String getStop_shopid() {
        return stop_shopid;
    }

    public void setStop_shopid(String stop_shopid) {
        this.stop_shopid = stop_shopid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocationModal getLocation() {
        return location;
    }

    public void setLocation(LocationModal location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    public String getDisplacement() {
        return displacement;
    }

    public void setDisplacement(String displacement) {
        this.displacement = displacement;
    }

    public PriceModal getPrices() {
        return prices;
    }

    public void setPrices(PriceModal prices) {
        this.prices = prices;
    }

    public String getCoverphoto() {
        return coverphoto;
    }

    public void setCoverphoto(String coverphoto) {
        this.coverphoto = coverphoto;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public CarSimpleModal() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.carid);
        dest.writeString(this.vin);
        dest.writeString(this.gearbox);
        dest.writeString(this.stop_shopid);
        dest.writeString(this.address);
        dest.writeParcelable(this.location, 0);
        dest.writeString(this.distance);
        dest.writeString(this.brandid);
        dest.writeString(this.modelid);
        dest.writeString(this.brand_name);
        dest.writeString(this.model_name);
        dest.writeString(this.displacement);
        dest.writeParcelable(this.prices, 0);
        dest.writeString(this.coverphoto);
    }

    protected CarSimpleModal(Parcel in) {
        this.carid = in.readString();
        this.vin = in.readString();
        this.gearbox = in.readString();
        this.stop_shopid = in.readString();
        this.address = in.readString();
        this.location = in.readParcelable(LocationModal.class.getClassLoader());
        this.distance = in.readString();
        this.brandid = in.readString();
        this.modelid = in.readString();
        this.brand_name = in.readString();
        this.model_name = in.readString();
        this.displacement = in.readString();
        this.prices = in.readParcelable(PriceModal.class.getClassLoader());
        this.coverphoto = in.readString();
    }

    public static final Creator<CarSimpleModal> CREATOR = new Creator<CarSimpleModal>() {
        public CarSimpleModal createFromParcel(Parcel source) {
            return new CarSimpleModal(source);
        }

        public CarSimpleModal[] newArray(int size) {
            return new CarSimpleModal[size];
        }
    };
}
