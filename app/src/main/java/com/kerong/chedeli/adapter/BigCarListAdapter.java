package com.kerong.chedeli.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kerong.chedeli.R;
import com.kerong.chedeli.data.modal.CarSimpleModal;

import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * Created by LiYaoHua on 2015/10/5.
 */
public class BigCarListAdapter extends BaseAdapter {
    private List<CarSimpleModal> itemList;
    private Context context;
    private FinalBitmap finalBitmap = null;
    public BigCarListAdapter(Context context,List<CarSimpleModal> itemList){
        this.context = context;
        this.itemList = itemList;
        finalBitmap = FinalBitmap.create(context);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_car_big,null);
            viewHolder.iv_car_photo = (ImageView) convertView.findViewById(R.id.iv_car_photo);
            viewHolder.tv_car_name = (TextView) convertView.findViewById(R.id.tv_car_name);
            viewHolder.tv_vin = (TextView) convertView.findViewById(R.id.tv_vin);
            viewHolder.tv_gearbox = (TextView) convertView.findViewById(R.id.tv_gearbox);
            viewHolder.tv_displacement = (TextView) convertView.findViewById(R.id.tv_displacement);
            viewHolder.tv_car_address = (TextView) convertView.findViewById(R.id.tv_car_address);
            viewHolder.tv_car_distance = (TextView) convertView.findViewById(R.id.tv_car_distance);
            viewHolder.tv_car_price = (TextView) convertView.findViewById(R.id.tv_car_price);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CarSimpleModal carSimpleModal = itemList.get(position);
        finalBitmap.display(viewHolder.iv_car_photo,carSimpleModal.getCoverphoto());
        viewHolder.tv_car_name.setText(carSimpleModal.getBrand_name()+" "+carSimpleModal.getModel_name());
        viewHolder.tv_vin.setText(carSimpleModal.getVin());
        viewHolder.tv_gearbox.setText(carSimpleModal.getGearbox().equals("1")?"自":"手");
        viewHolder.tv_displacement.setText("排量："+carSimpleModal.getDisplacement());
        viewHolder.tv_car_address.setText(carSimpleModal.getAddress());
        viewHolder.tv_car_distance.setText(carSimpleModal.getDistance()+"米");
        viewHolder.tv_car_price.setText(carSimpleModal.getPrices().getDay()+"/天");
        return convertView;
    }

    static class ViewHolder{
        ImageView iv_car_photo;
        TextView tv_car_name;
        TextView tv_vin;
        TextView tv_gearbox;
        TextView tv_displacement;
        TextView tv_car_address;
        TextView tv_car_distance;
        TextView tv_car_price;
    }
}
