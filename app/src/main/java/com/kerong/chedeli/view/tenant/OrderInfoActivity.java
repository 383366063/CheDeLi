package com.kerong.chedeli.view.tenant;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.kerong.chedeli.CheDeLiApplication;
import com.kerong.chedeli.R;
import com.kerong.chedeli.data.TestData;
import com.kerong.chedeli.data.modal.OrderInfoModal;
import com.kerong.chedeli.data.modal.OrderModal;
import com.kerong.chedeli.data.modal.OrderReadResult;
import com.kerong.chedeli.data.modal.OrderResultModal;
import com.kerong.chedeli.data.modal.ResultModal;
import com.kerong.chedeli.view.BaseActivity;
import com.kerong.chedeli.view.UIHelper;

import net.tsz.afinal.FinalBitmap;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.http.AjaxCallBack;
import net.tsz.afinal.http.AjaxParams;

import org.w3c.dom.Text;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 * 我的订单列表中的订单详情
 * Created by LiYaoHua on 2015/10/6.
 */
public class OrderInfoActivity extends BaseActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.iv_car_photo)
    ImageView iv_car_photo;

    @Bind(R.id.tv_vin)
    TextView tv_vin;

    @Bind(R.id.tv_car_name)
    TextView tv_car_name;

    @Bind(R.id.tv_back_time)
    TextView tv_back_time;

    @Bind(R.id.no_date_layout)
    RelativeLayout no_date_layout;

    @Bind(R.id.tv_get_time)
    TextView tv_get_time;

    @Bind(R.id.tv_use_time)
    TextView tv_use_time;

    @Bind(R.id.tv_load_text)
    TextView tv_load_text;

    @Bind(R.id.tv_order_status)
    TextView tv_order_status;

    @Bind(R.id.bt_submit_order)
    Button bt_submit_order;

    FinalBitmap finalBitmap = null;
    String orderId = null;
    OrderInfoModal infoModaal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_info);
        ButterKnife.bind(this);
        finalBitmap = FinalBitmap.create(this);
        initToolBar(toolbar);
        orderId = getIntent().getStringExtra("orderid");
        init();
    }

    private void init(){
        String userPhone = CheDeLiApplication.getUserMoal().getUsrmobile();
        String url = "http://cdlmapi.0371e.cn/v1/orderdetail";
        FinalHttp finalHttp = new FinalHttp();
        finalHttp.configCookieStore(CheDeLiApplication.cookieStore);
        AjaxParams ajaxParams = new AjaxParams();
        ajaxParams.put("usrmobile", userPhone);
        ajaxParams.put("orderid", orderId);
        finalHttp.get(url, ajaxParams, new AjaxCallBack<String>() {
            @Override
            public void onSuccess(String s) {
                try {
                    OrderReadResult resultModal = new Gson().fromJson(s,OrderReadResult.class);
                    if(resultModal.getStatus() == 0){
                        OrderInfoModal orderModal = resultModal.getResults();
                        infoModaal = orderModal;
                        finalBitmap.display(iv_car_photo, orderModal.getCarinfo().getCoverphoto());
                        tv_vin.setText(orderModal.getCarinfo().getVin());
                        tv_car_name.setText(orderModal.getCarinfo().getBrand_name());
                        tv_get_time.setText(orderModal.getUseStartTime());
                        tv_back_time.setText(orderModal.getUseEndTime());
                        tv_use_time.setText(CheDeLiApplication.formatTime(Long.parseLong(orderModal.getUse_time())));
                        tv_order_status.setText(CheDeLiApplication.STATUS_MAP.get(orderModal.getOrder_state()));
                        if(!orderModal.getOrder_state().equals("1")&&!orderModal.getOrder_state().equals("3")){
                            bt_submit_order.setVisibility(View.GONE);
                        }
                        no_date_layout.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    no_date_layout.setVisibility(View.VISIBLE);
                    tv_load_text.setText("数据记载失败");
                }
            }

            @Override
            public void onFailure (Throwable t,int errorNo, String strMsg){
                no_date_layout.setVisibility(View.VISIBLE);
                tv_load_text.setText("数据记载失败");
            }
            }

            );
        }

        @OnClick(R.id.bt_submit_order)
    public void onSubmitClick(View view){
            String url = "http://cdlmapi.0371e.cn/v1/revokeorder";
            AjaxParams params = new AjaxParams();
            params.put("usrmobile",CheDeLiApplication.getUserMoal().getUsrmobile());
            params.put("orderid",orderId);
            if(infoModaal.getOrder_state().equals("1")){
                //执行取消
                params.put("opcode","1");
            }else if(infoModaal.getOrder_state().equals("3")){//执行删除
                params.put("opcode","2");
            }
            FinalHttp finalHttp = new FinalHttp();
            finalHttp.configCookieStore(CheDeLiApplication.cookieStore);
            finalHttp.get(url, params, new AjaxCallBack<String>() {
                @Override
                public void onSuccess(String s) {
                    try {
                        ResultModal resultModal = new Gson().fromJson(s,ResultModal.class);
                        if(resultModal.getStatus() == 0){
                            UIHelper.showMessage("操作成功");
                            finish();
                        }else{
                            UIHelper.showMessage("操作失败");
                        }
                    }catch (Exception e){
                        UIHelper.showMessage("操作失败");
                    }
                }

                @Override
                public void onFailure(Throwable t, int errorNo, String strMsg) {
                    UIHelper.showMessage(strMsg);
                }
            });
    }
}
